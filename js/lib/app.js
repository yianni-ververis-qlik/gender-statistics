/*
 * @owner yianni.ververis@qlik.com
 *
 */
var me = {
		obj: {
			qlik: null,
			app: null,
			angularApp: null,
			model: []
		}
	};

me.init = function () {
	me.config = {
			host: null,
			prefix: null,
			port:null,
			isSecure: true
	};
	me.vars = {
		v: "1.13",
		config: {
			stagingQlik: {
				host: 'demos.qlik.com',
				prefix: "/",
				port: 443,
				isSecure: true,
				id: '5854e229-9906-4d19-9e0e-220a5e7c22b4'
			},
			devUN: {
				host: 'viz.dev.un.org',
				prefix: "/visualization/",
				port: null,
				isSecure: true,
				id: 'f2c08d76-1bae-44ab-b3b9-5d47556e325f'
			},
			prodUN: {
				host: 'viz.unite.un.org',
				prefix: "/visualization/",
				port: null,
				isSecure: true,
				id:'63172c31-bf2d-4083-bc50-eb19e14978b5'
                //id: '34f5aa2c-a487-4026-9992-092ea8eed716'
			},
		}
	};
	me.config = me.vars.config.stagingQlik;
}

me.boot = function () {
	me.init();

	me.obj.app = me.obj.qlik.openApp(me.config.id, me.config);

	me.events();
};

me.events = function () {
	$( document ).ready(function() {
		$(".container").height($("body").height() - 50);
	});

	$(window).resize(function() {
	    $(".container").height($("body").height());
	});

	console.log('%c App ' + me.vars.v + ': ', 'color: red', 'Loaded!');

};

app = me;
